from expression_classes import *


class Parser:
    """
    This class parses the math expression and returns instance of class Expression
    which contains tree structure for all operations which are in the given expression
    """
    def __init__(self, input_string):
        self.input_string = input_string
        self.cnt = 0

    # Returns next symbol from the input string and increment counter
    def read_next(self):
        next_symbol = '\0'
        if self.cnt < len(self.input_string):
            next_symbol = self.input_string[self.cnt]
        self.cnt += 1
        return next_symbol

    # To decrease counter for cases when we are parsing factors, meet the '+' sign and returns to the
    # execution of parse terms method with operator which is already red
    def read_back(self):
        if self.cnt > 0:
            self.cnt -= 1

    # Root method for parsing whole expression
    def parse(self):
        return self.parse_relation()

    # Parse relation which is: term [ (<, >, =) term ]
    def parse_relation(self):
        left = self.parse_term()
        operator = self.read_next()
        # If operator is end of string then we have no relation, only arithmetic operations
        if operator != '\0' and operator != ')':
            right = self.parse_term()
            if operator == '<':
                return Less(left, right)
            elif operator == '>':
                return Greater(left, right)
            elif operator == '=':
                return Equal(left, right)
            else:
                print(f'Unexpected operator {operator}')
                return left
        else:
            return left

    # Parse relation which is: factor { (+, -) factor }
    # We can have here a lot of 'factor's
    def parse_term(self):
        result = self.parse_factor()
        # Parse until end of terms
        while True:
            operator = self.read_next()
            if operator == '\0':
                break
            if operator != '+' and operator != '-':
                self.read_back()
                break
            right = self.parse_factor()
            if operator == '+':
                result = Sum(result, right)
            elif operator == '-':
                result = Minus(result, right)
            else:
                print(f'Term ended {operator}')
                self.read_back()
                return result
        return result

    # Parse factor which is: primary { * primary}
    def parse_factor(self):
        result = self.parse_primary()
        # Parse until end of factors
        while True:
            operator = self.read_next()
            if operator == '\0':
                break
            if operator != '*':
                self.read_back()
                break
            right = self.parse_primary()
            if operator == '*':
                result = Multiply(result, right)
            else:
                print(f'Factor ended {operator}')
                self.read_back()
                return result
        return result

    # Parse primary units which are either integer or '(' expression ')' unit
    def parse_primary(self):
        next_token = self.read_next()
        if next_token == '(':
            result = self.parse()
            return result
        elif next_token.isdigit():
            result = 0
            while next_token.isdigit():
                result = result * 10 + int(next_token)
                next_token = self.read_next()
            self.read_back()
            return Integer(result)
        else:
            print(f'Unexpected symbol {next_token} at position {self.cnt}')
            return self.parse_primary()
