from parser import Parser


if __name__ == '__main__':
    input_string = input()
    parser = Parser(input_string)
    result = parser.parse()
    print(result.evaluate())
