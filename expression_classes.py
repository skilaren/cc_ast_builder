class Expression:
    def evaluate(self):
        """
        Override this method in leaves units
        :return: in leaves should return numeric value - result of operation
        """
        pass


class Binary(Expression):
    left = Expression()
    right = Expression()

    def __init__(self, left, right):
        """
        Creates Sum object declaring left and right expressions
        :param left: left expression
        :param right: right expression
        """
        self.left = left
        self.right = right


class Primary(Expression):
    pass


class Relation(Binary):
    pass


class Term(Binary):
    pass


class Factor(Binary):
    pass


class Integer(Primary):

    def __init__(self, value):
        self.value = value

    def evaluate(self):
        return self.value


class Parenthesized(Primary):
    pass


# Classes for relations
class Less(Relation):
    def evaluate(self):
        if self.left.evaluate() < self.right.evaluate():
            return 1
        else:
            return 0


class Greater(Relation):
    def evaluate(self):
        if self.left.evaluate() > self.right.evaluate():
            return 1
        else:
            return 0


class Equal(Relation):
    def evaluate(self):
        if self.left.evaluate() == self.right.evaluate():
            return 1
        else:
            return 0


# Classes for arithmetic operations
class Sum(Term):
    def evaluate(self):
        return self.left.evaluate() + self.right.evaluate()


class Minus(Term):
    def evaluate(self):
        return self.left.evaluate() - self.right.evaluate()


class Multiply(Factor):
    def evaluate(self):
        return self.left.evaluate() * self.right.evaluate()
